package br.com.syonet.utilitarios;

import java.lang.reflect.Field;
import java.text.Normalizer;

public class UtilitarioEntidades {

	public static void uperCaseAll(Object object) {
		for (Field campo : object.getClass().getDeclaredFields()) {
			if(campo.getType().equals(String.class)) {
				campo.setAccessible(true);
				try {
					if(campo.get(object) != null) {
						campo.set(object, campo.get(object).toString().toUpperCase());
					}
				} catch (IllegalArgumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
	
	
	public static void removerAcentos(Object object) {
		for (Field campo : object.getClass().getDeclaredFields()) {
			if(campo.getType().equals(String.class)) {
				campo.setAccessible(true);
				try {
					if(campo.get(object) != null) {
						campo.set(object, Normalizer.normalize(campo.get(object).toString(), Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", ""));
					}
				} catch (IllegalArgumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

}
