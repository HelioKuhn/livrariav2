package br.com.syonet.entidades;

public class LivroDTO {

	private String titulo;


	private String tituloOriginal;


	private String nomeEditora;


	private Integer numeroPaginas;

	
	private Integer numeroEdicao;
	
	private String nomeAutor;

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getTituloOriginal() {
		return tituloOriginal;
	}

	public void setTituloOriginal(String tituloOriginal) {
		this.tituloOriginal = tituloOriginal;
	}

	public String getNomeEditora() {
		return nomeEditora;
	}

	public void setNomeEditora(String nomeEditora) {
		this.nomeEditora = nomeEditora;
	}

	public Integer getNumeroPaginas() {
		return numeroPaginas;
	}

	public void setNumeroPaginas(Integer numeroPaginas) {
		this.numeroPaginas = numeroPaginas;
	}

	public Integer getNumeroEdicao() {
		return numeroEdicao;
	}

	public void setNumeroEdicao(Integer numeroEdicao) {
		this.numeroEdicao = numeroEdicao;
	}

	public String getNomeAutor() {
		return nomeAutor;
	}

	public void setNomeAutor(String nomeAutor) {
		this.nomeAutor = nomeAutor;
	}
	
	
	public Livro getLivro(LivroDTO livroDTO) {
		Livro livro = new Livro();
		livro.setNomeEditora(livro.getNomeEditora());
		livro.setNumeroEdicao(livroDTO.getNumeroEdicao());
		livro.setNumeroPaginas(livroDTO.getNumeroPaginas());
		livro.setTitulo(livroDTO.getTitulo());
		livro.setTituloOriginal(livroDTO.getTituloOriginal());
		return livro;
	}

	
	

}
