package br.com.syonet.entidades;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "syo_livro")
public class Livro {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	
	private String titulo;


	private String tituloOriginal;


	private String nomeEditora;


	private Integer numeroPaginas;

	
	private Integer numeroEdicao;

	@ManyToOne
	Autor autor;
	
	public Livro() {
		
	}
	
	private Livro(LivroBuilder livroBuilder) {
		this.id = livroBuilder.id;
		this.titulo = livroBuilder.titulo;
		this.tituloOriginal = livroBuilder.tituloOriginal;
		this.autor = livroBuilder.autor;
		this.nomeEditora = livroBuilder.nomeEditora;
		this.numeroPaginas = livroBuilder.numeroPaginas;
		this.numeroEdicao = livroBuilder.numeroEdicao;
	}

	public Autor getAutor() {
		return autor;
	}

	public void setAutor(Autor autor) {
		this.autor = autor;
	}

	public Integer getId() {
		return id;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public void setTituloOriginal(String tituloOriginal) {
		this.tituloOriginal = tituloOriginal;
	}

	public void setNomeEditora(String nomeEditora) {
		this.nomeEditora = nomeEditora;
	}

	public void setNumeroPaginas(Integer numeroPaginas) {
		this.numeroPaginas = numeroPaginas;
	}

	public void setNumeroEdicao(Integer numeroEdicao) {
		this.numeroEdicao = numeroEdicao;
	}

	public String getTitulo() {
		return titulo;
	}

	public String getTituloOriginal() {
		return tituloOriginal;
	}

	public String getNomeEditora() {
		return nomeEditora;
	}

	public Integer getNumeroPaginas() {
		return numeroPaginas;
	}

	public Integer getNumeroEdicao() {
		return numeroEdicao;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	public static class LivroBuilder {

		private Integer id;
		private String titulo;
		private String tituloOriginal;
		private String nomeEditora;
		private Integer numeroPaginas;
		private Integer numeroEdicao;
		private Autor autor;

		public LivroBuilder id(Integer id) {
			this.id = id;
			return this;
		}

		public LivroBuilder titulo(String titulo) {
			this.titulo = titulo;
			return this;
		}

		public LivroBuilder tituloOriginal(String tituloOriginal) {
			this.tituloOriginal = tituloOriginal;
			return this;
		}

		public LivroBuilder autores(Autor autor) {
			this.autor = autor;
			return this;
		}

		public LivroBuilder momeEditora(String nomeEditora) {
			this.nomeEditora = nomeEditora;
			return this;
		}

		public LivroBuilder numeroPaginas(Integer numeroPaginas) {
			this.numeroPaginas = numeroPaginas;
			return this;
		}

		public LivroBuilder numeroEdicao(Integer numeroEdicao) {
			this.numeroEdicao = numeroEdicao;
			return this;
		}

		public Livro buid() {
			return new Livro(this);
		}

	}

}
