package br.com.syonet.entidades;

public class AutorDTO {

	private String nome;

	private String dataNascimento;

	private String cidade;

	public AutorDTO() {

	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(String dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public Autor getAutor(AutorDTO autorDTO) {
		Autor autor = new Autor();
		autor.setNome(autorDTO.getNome());
		autor.setDataNascimento(autorDTO.getDataNascimento());
		autor.setCidade(autorDTO.getCidade());
		return autor;
	}

}
