package br.com.syonet.entidades;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "syo_autor")
public class Autor {

	
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	@Id
	@Column(name = "nome_autor")
	private String nomeCompleto;

	
	private String dataNascimento;


	private String cidade;

	public Autor() {

	}

	private Autor(AutorBuilder autorBuilder) {
		this.id = autorBuilder.id;
		this.nomeCompleto = autorBuilder.nomeCompleto;
		this.dataNascimento = autorBuilder.dataNascimento;
		this.cidade = autorBuilder.cidade;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nomeCompleto;
	}

	public void setNome(String nome) {
		this.nomeCompleto = nome;
	}

	public String getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(String dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	@Override
	public String toString() {
		return "Autor [id=" + id + ", nome=" + nomeCompleto +   ", dataNascimento=" + dataNascimento
				+ ", cidade=" + cidade + "]";
	}
	

	public static class AutorBuilder {

		private Integer id;
		private String nomeCompleto;
		private String dataNascimento;
		private String cidade;

		public AutorBuilder id(Integer id) {
			this.id = id;
			return this;
		}

		public AutorBuilder nome(String nome) {
			this.nomeCompleto = nome;
			return this;
		}

		public AutorBuilder dataNascimento(String dataNascimento) {
			this.dataNascimento = dataNascimento;
			return this;
		}

		public AutorBuilder cidade(String cidade) {
			this.cidade = cidade;
			return this;
		}

		public Autor buid() {
			return new Autor(this);
		}

	}

}
