package br.com.syonet.exceptions;

public class AutorExeception extends Exception{

	private static final long serialVersionUID = 1L;
	
	public AutorExeception(String mensagem, Throwable causa) {
		super(mensagem, causa);
	}

}
