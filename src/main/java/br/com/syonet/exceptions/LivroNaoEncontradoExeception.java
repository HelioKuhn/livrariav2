package br.com.syonet.exceptions;

public class LivroNaoEncontradoExeception extends Exception{

	private static final long serialVersionUID = 1L;
	
	public LivroNaoEncontradoExeception(String mensagem) {
		super(mensagem);
	}

}
