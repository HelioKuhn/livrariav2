package br.com.syonet.repository;

import java.util.List;
import java.util.Objects;

import javax.enterprise.context.ApplicationScoped;
import javax.transaction.Transactional;

import br.com.syonet.entidades.Livro;
import br.com.syonet.exceptions.LivroNaoEncontradoException;
import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase;

@ApplicationScoped
public class LivroRepository implements ILivroRepository, PanacheRepositoryBase<Livro, Integer> {

	@Override
	@Transactional
	public void inserirLivro(Livro livro) {
		System.out.println("chegou aqui");
		if(Objects.isNull(livro.getId())) {
			this.persist(livro);
		}
		this.findByIdOptional(livro.getId()).ifPresentOrElse( a ->{
		System.out.println("Livro com Id " + livro.getId() + " já está cadastrado");
		}, () -> this.persist(livro));
	}

	@Override
	@Transactional
	public void edit(Livro livro) {
		this.findByIdOptional(livro.getId()).ifPresentOrElse(livroAux ->{
			livroAux.setTitulo(livro.getTitulo());
			livroAux.setTituloOriginal(livro.getTituloOriginal());
			livroAux.setNomeEditora(livro.getNomeEditora());
			livroAux.setNumeroEdicao(livro.getNumeroEdicao());
			livroAux.setNumeroPaginas(livro.getNumeroPaginas());
		},() -> new LivroNaoEncontradoException("Livro não encontrado"));

	}

	@Override
	@Transactional
	public void removeLivro(Livro livro) {
		this.findByIdOptional(livro.getId()).ifPresentOrElse(livroAux ->{
			this.delete(livroAux);
		},() -> new LivroNaoEncontradoException("Livro não encontrado"));
	}

	@Override
	public Livro findByName(String nomeLivro) {
		return null;
	}

	@Override
	public List<Livro> findAllLivros() {
		return this.findAll().list();
	}

	@Override
	public Livro findbyIdLivro(Integer id) {
		return this.findById(id);
	}

}
