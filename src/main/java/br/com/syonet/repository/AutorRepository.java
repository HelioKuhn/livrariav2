package br.com.syonet.repository;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.transaction.Transactional;

import br.com.syonet.entidades.Autor;
import br.com.syonet.exceptions.AutorNaoEncontradoException;
import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase;

@ApplicationScoped
public class AutorRepository implements IAutorRepository, PanacheRepositoryBase<Autor, String> {

	@Override
	@Transactional
	public void inserirAutor(Autor autor) {
		System.out.println("chegou aqui");
		this.findByIdOptional(autor.getNome()).ifPresentOrElse( a ->{
		System.out.println("Autor com o nome " + autor.getNome() + " já está cadastrado");
		}, () -> this.persist(autor));
	}

	@Override
	@Transactional
	public void edit(Autor autor) {
		this.findByIdOptional(autor.getNome()).ifPresentOrElse(autorAux ->{
			autorAux.setNome(autor.getNome());
			autorAux.setDataNascimento(autor.getDataNascimento());
			autorAux.setCidade(autor.getCidade());
		},() -> new AutorNaoEncontradoException("Autor não encontrado"));

	}

	@Override
	@Transactional
	public void removeAutor(Autor autor) {
		this.findByIdOptional(autor.getNome()).ifPresentOrElse(autorAux ->{
			this.delete(autorAux);
		},() -> new AutorNaoEncontradoException("Autor não encontrado"));
	}

	@Override
	public Autor findByName(String nomeAutor) {
		return this.findById(nomeAutor);
	}

	@Override
	public List<Autor> findAllAutores() {
		return this.findAll().list();
	}
	
	@Override
	@Transactional
	public void removeAll() {
	    this.deleteAll();
	}

}
