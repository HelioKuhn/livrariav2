package br.com.syonet.repository;

import java.util.List;

import br.com.syonet.entidades.Autor;


public interface IAutorRepository {
	
	void inserirAutor(Autor autor);
	void edit(Autor autor);
	void removeAutor(Autor autor);
	Autor findByName(String String);
	List<Autor> findAllAutores();
	void removeAll();
	
}
