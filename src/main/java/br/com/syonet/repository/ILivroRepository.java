package br.com.syonet.repository;

import java.util.List;

import br.com.syonet.entidades.Autor;
import br.com.syonet.entidades.Livro;


public interface ILivroRepository {
	
	void inserirLivro(Livro livro);
	void edit(Livro Livro);
	void removeLivro(Livro livro);
	Livro findByName(String String);
	Livro findbyIdLivro(Integer id);
	List<Livro> findAllLivros();
	
}
