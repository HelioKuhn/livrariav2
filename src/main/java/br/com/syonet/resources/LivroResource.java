package br.com.syonet.resources;

import java.util.List;
import java.util.Objects;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.jboss.resteasy.annotations.jaxrs.QueryParam;

import br.com.syonet.entidades.Autor;
import br.com.syonet.entidades.Livro;
import br.com.syonet.entidades.LivroDTO;
import br.com.syonet.services.AutorService;
import br.com.syonet.services.LivroService;

@Path("/api")
public class LivroResource {

	@Inject
	LivroService livroService;
	
	@Inject
	AutorService autorService;

	@POST
	@Path("/livro")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@APIResponse(responseCode = "201", description = "Livro Criado")
	@APIResponse(responseCode = "404", description = "Falha na requisição")
	public Response salvarLivro(@Valid LivroDTO livroDTO) {
		Autor autor = autorService.findyByName(livroDTO.getNomeAutor());
		if(Objects.isNull(autor)) {
			return Response.noContent().status(404).build();
		}
		Livro livro = livroDTO.getLivro(livroDTO);
		livro.setAutor(autor);
		livroService.salvarLivro(livro);
		return Response.ok().status(201).build();
	}
	
	@PATCH
	@Path("/livro")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response editar(@Valid LivroDTO livroDTO, @QueryParam("id") Integer id) {
		Livro livro = livroService.findById(id);
		if(Objects.isNull(livro)) {
			return Response.noContent().status(404).build();
		}

		Autor autor = autorService.findyByName(livroDTO.getNomeAutor());
		if(!Objects.isNull(autor)){
			livro.setAutor(autor);
		}
		return Response.ok(autor).status(200).build();
	}
	
	@DELETE
	@Path("/livro")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response remove(@QueryParam(value = "id") Integer id) {
		Livro livro = livroService.findById(id);
		if(Objects.isNull(livro)) {
			return Response.noContent().status(404).build();
		}
		livroService.remove(livro);
		return Response.ok().status(200).build();
	}

	@GET
	@Path("/livro/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response findbyId(@QueryParam(value = "id") Integer id) {
		System.out.println(id);
		Livro livro = livroService.findById(id);
		if(Objects.isNull(livro)) {
			return Response.noContent().status(200).build();
		}
		return Response.ok(livro).build();
	}
	
	@GET
	@Path("/livros")
	@Produces(MediaType.APPLICATION_JSON)
	public Response findall() {
		List<Livro> livros = livroService.findAll();
		if(Objects.isNull(livros)) {
			return Response.noContent().status(200).build();
		}
		return Response.ok(livros).build();
	}

}
