package br.com.syonet.resources;

import java.util.List;
import java.util.Objects;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.jboss.resteasy.annotations.jaxrs.QueryParam;

import br.com.syonet.entidades.Autor;
import br.com.syonet.entidades.AutorDTO;
import br.com.syonet.services.AutorService;

@Path("/api")
public class AutorResource {

	@Inject
	AutorService autorService;

	@POST
	@Path("/autor")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response salvarAutor(AutorDTO autorDTO) {
		Autor autor = autorDTO.getAutor(autorDTO);
		autorService.salvarAutor(autor);
		return Response.ok().status(201).build();
	}
	
	@PATCH
	@Path("/autor{nomeAutor}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response editar(AutorDTO autorDTO, @QueryParam("nomeAutor") String nome) {
		Autor autor = autorService.findyByName(nome);
		if(Objects.isNull(autor)) {
			return Response.noContent().status(404).build();
		}
		
		autor = autorDTO.getAutor(autorDTO);
		autorService.ediarAutor(autor);
		return Response.ok().status(200).build();
	}
	
	@DELETE
	@Path("/autor/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response remove(@QueryParam(value = "NomeAutor") String nomeAutor) {
		Autor autor = autorService.findyByName(nomeAutor);
		if(Objects.isNull(autor)) {
			return Response.noContent().status(404).build();
		}
		autorService.remove(autor);
		return Response.ok().status(200).build();
	}

	@GET
	@Path("/autor/")
	@Produces(MediaType.APPLICATION_JSON)
	public Response findbyId(@QueryParam(value = "nomeCompleto") String nomeCompleto) {
		System.out.println(nomeCompleto);
		Autor autor = autorService.findyByName(nomeCompleto);
		if(Objects.isNull(autor)) {
			return Response.noContent().status(200).build();
		}
		return Response.ok(autor).build();
	}
	
	@GET
	@Path("/autor/autores")
	@Produces(MediaType.APPLICATION_JSON)
	public Response findall() {
		List<Autor> autores = autorService.findAll();
		if(autores.isEmpty()) {
			return Response.noContent().status(204).build();
		}
		return Response.ok(autores).status(200).build();
	}

}
