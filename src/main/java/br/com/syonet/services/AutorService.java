package br.com.syonet.services;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import br.com.syonet.entidades.Autor;
import br.com.syonet.repository.IAutorRepository;
import br.com.syonet.utilitarios.UtilitarioEntidades;

@ApplicationScoped
public class AutorService {

	@Inject
	IAutorRepository iAutorRepository;

	public void salvarAutor(Autor autor) {

		UtilitarioEntidades.removerAcentos(autor);
		UtilitarioEntidades.uperCaseAll(autor);
		iAutorRepository.inserirAutor(autor);
	}

	public Autor findyByName(String nome) {
		return iAutorRepository.findByName(nome);

	}

	public List<Autor> findAll() {
		return iAutorRepository.findAllAutores();
	}

	public void remove(Autor autor) {
		iAutorRepository.removeAutor(autor);
	}
	
	public void ediarAutor(Autor autor) {
		UtilitarioEntidades.removerAcentos(autor);
		UtilitarioEntidades.uperCaseAll(autor);
		iAutorRepository.edit(autor);
	}


}
