package br.com.syonet.services;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import br.com.syonet.entidades.Autor;
import br.com.syonet.entidades.Livro;
import br.com.syonet.repository.ILivroRepository;
import br.com.syonet.utilitarios.UtilitarioEntidades;

@ApplicationScoped
public class LivroService {

	@Inject
	ILivroRepository iLivroRepository;

	public void salvarLivro(Livro livro) {

		UtilitarioEntidades.removerAcentos(livro);
		UtilitarioEntidades.uperCaseAll(livro);
		iLivroRepository.inserirLivro(livro);
	}

	public Autor findyByName(String nome) {
		return null;

	}

	public List<Livro> findAll() {
		return iLivroRepository.findAllLivros();
	}

	public void remove(Livro livro) {
		iLivroRepository.removeLivro(livro);
	}
	
	public void editarLivro(Livro livro) {
		UtilitarioEntidades.removerAcentos(livro);
		UtilitarioEntidades.uperCaseAll(livro);
		iLivroRepository.edit(livro);
	}
	
	public Livro findById(Integer id) {
		return iLivroRepository.findbyIdLivro(id);
	}

}
