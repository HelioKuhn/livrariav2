package resources;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.nio.charset.Charset;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.core.MediaType;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestMethodOrder;

import br.com.syonet.entidades.Autor;
import br.com.syonet.entidades.Autor.AutorBuilder;
import br.com.syonet.repository.AutorRepository;
import br.com.syonet.services.AutorService;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.parsing.Parser;

@QuarkusTest
@TestInstance( TestInstance.Lifecycle.PER_CLASS )
@TestMethodOrder( OrderAnnotation.class )
public class AutorResourceTest {
    
    @Inject
    AutorService autorService;
    
    @Inject
    AutorRepository autorRepository;
    
    
    @BeforeEach
    void configuration() {
        RestAssured.requestSpecification = RestAssured
                .given()
                .contentType(ContentType.JSON )
                .accept( ContentType.JSON );
        RestAssured.config().getEncoderConfig()
                .defaultCharsetForContentType(
                        Charset.forName( "UTF-8" ),
                        MediaType.APPLICATION_JSON );
        RestAssured.config().getDecoderConfig()
                .defaultCharsetForContentType(
                        Charset.forName( "UTF-8" ),
                        MediaType.APPLICATION_JSON );
        RestAssured.requestSpecification.contentType( ContentType.JSON );
        RestAssured.defaultParser = Parser.JSON;
    }
    
    @Test
    @Order( 1 )
    public void testBuscarAutores() {
        List<Autor> autores = 
                RestAssured.given()
                .when()
                .basePath("/api/autor/autores")
                .get()
                .then()
                .statusCode(200)
                .extract().as(List.class);
                assertTrue(autores.size() > 0);
                System.out.println(autores.size());
        RestAssured.given()
                .when()
                .basePath("/api/autor/autores")
                .get()
                .then()
                .statusCode(204)
                .extract().as(List.class);
                 assertTrue(autores.size() == 0);
                
                
                
    }
	
	
	 @Test
	 @Order( 2 )
    public void testSalvarAutor() {
        Autor autor =  new AutorBuilder()
                .cidade("Cidade")
                .dataNascimento("16/01/1991")
                .nome("Autor 2")
                .buid();
        autorService.salvarAutor(autor);
        RestAssured.given()
            .when()
            .contentType(ContentType.JSON)
            .accept(ContentType.JSON)
            .with().body( autor )
            .post( "api/autor" )
            .then()
            .statusCode( 201 );
        
        
    }
	 
	 @AfterAll
	    void limpaBase() {
	     autorRepository.removeAll();
	    }

}
